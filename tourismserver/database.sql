CREATE DATABASE tourismapp;

-- set extention
CREATE TABLE users(
    -- user_id -> this is the name of the column being defined
    -- uuid -> data types used as unique identifiers for records in a database
    -- SERIAL -> is a data type in PostgreSQL that is often used to create an auto-incrementing integer column
    -- PRIMARY KEY -> a primary key is a column or combination of columns that uniquely identifies each row in the table.
    -- DEFAULt uuid_generate_v4 -> This specifies that when a new row is inserted into the table and no value is provided for user_id, the uuid_generate_v4 function will be used to generate a new UUID for the user_id column
    user_id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    -- user_name, user_email, user_password -> This is the name of the column being defined
    -- VARCHAR ->  data types can store variable-length character strings
    -- (255) -> the description column can store text values of up to 255 characters in length
    -- NOT NULL -> column must have a value for every row in the table, and it cannot be empty or null
    user_name VARCHAR(255) NOT NULL,
    user_email VARCHAR(255) NOT NULL,
    user_password VARCHAR(255) NOT NULL
);

-- INSERT fake user

 
INSERT INTO users (user_name, user_email, user_password) VALUES ('safiah', 'safiahnadiah8891@gmail.com', 'safiah8891');
    