// .Pool -> classes which is used to create and manage a pool of database connections.Connection pooling is a common technique in database programming to efficiently manage and reuse database connections, which can improve the performance and scalability of database operations in web applications.
const Pool = require("pg").Pool

const pool = new Pool({
    user: "postgres",
    password: "safiah8891",
    host: "localhost",
    port: "5432",
    database: 'tourismapp'
});

module.exports = pool;