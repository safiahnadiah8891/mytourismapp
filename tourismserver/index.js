const express = require("express");
const app = express();
const cors = require("cors");

//MIDDLEWARE

// is a middleware function in Express.js that is used to parse incoming JSON data from client requests
app.use(express.json());
// is a method used to mount middleware functions in your application's request handling pipeline
app.use(cors());

// ROUTES

// register and login routes
app.use("/auth", require("./routes/jwtAuth"));

// dashboard route
app.use("/dashboard", require("./routes/dashboard"));



app.listen(5000, () => {
    console.log("server has started on port 5000");
});