
import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import Dashboard from './components/dashboard';
import Login from './components/login';
import Register from './components/register';
import './App.css';
import Cookies from 'js-cookie';
import { ToastContainer } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";
import axios from 'axios'; // Import Axios

function App () {

  const [isAuthenticated, setIsAuthenticated] = useState(false);

  const setAuth = boolean => {
    setIsAuthenticated(boolean);
  };

  // async function isAuth() {
  //   try {
  //     const response = await fetch("http://localhost:5000/auth/verify", {
  //       method: "GET",
  //       headers: { token: Cookies.get('token') }
  //     });

  //     const parseRes = await response.json()

  //     parseRes === true ? setIsAuthenticated(true) : setIsAuthenticated(false);
      
  //   } catch (error) {
  //     console.error(error.message);
  //   }
  // }

  async function isAuth() {
    try {
      const response = await axios.get("http://localhost:5000/auth/verify", {
        headers: { token: Cookies.get('token') }
      });

      const parseRes = response.data;

      parseRes === true ? setIsAuthenticated(true) : setIsAuthenticated(false);

    } catch (error) {
      console.error(error.message);
    }
  }

  useEffect(() => {
    isAuth();
  });
    return (
      <>
        <ToastContainer />
        <Router>
          <div className="App">
            <Routes>
              <Route exact path='/login' element={!isAuthenticated ? (< Login setAuth={setAuth} />) : (
                <Navigate to="/dashboard" />)}></Route>
              <Route exact path='/register' element={!isAuthenticated ? (< Register setAuth={setAuth}  />) : (
                <Navigate to="/login"  />)}></Route>
              <Route exact path='/dashboard' element={isAuthenticated ? (< Dashboard setAuth={setAuth}  />) : (
                <Navigate to="/login"  />)}></Route>
            </Routes>
          </div>
        </Router>
      </>
    );
  
}

export default App;