import React, {useState} from 'react';
import { Typography, TextField, Button, Container, Grid } from '@mui/material';
import Cookies from 'js-cookie';
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';
import axios from 'axios';

function Login({setAuth}) {
    const [inputs, setInputs] = useState({
        email: "",
        password: ""
    })

    const { email, password } = inputs;

    const onChange = e => {
        setInputs({ 
            ...inputs,
            [e.target.name]: e.target.value
        })
    }

    const onSubmitForm = async (e) => {
        e.preventDefault(); // to prevent refresh

        try {
            const body = { email, password };

            const response = await axios.post("http://localhost:5000/auth/login", body, {
                headers: { "Content-Type": "application/json" },
            });

            const parseRes = response.data;

            if (parseRes.jwtToken) {
                const token = parseRes.jwtToken;
                Cookies.set('token', token, { expires: 7 });

                setAuth(true);

                toast.success('Login Successfully!', {
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                setAuth(false);
                toast.error(parseRes);
            }
        } catch (error) {
            console.error(error.message);
        }
    };

    // const onSubmitForm = async (e) => {
    //     e.preventDefault() // to prevent from refresh

    //     try {
    //         const body = { email, password }
            
    //         const response = await fetch("http://localhost:5000/auth/login", {
    //             method: "POST",
    //             headers: { "Content-Type": "application/json" },
    //             body: JSON.stringify(body)
    //         });

    //         const parseRes = await response.json()

    //         if(parseRes.jwtToken) {
    //             const token = parseRes.jwtToken; // Replace with your actual token
    //             Cookies.set('token', token, { expires: 7 }); // 'token' is the cookie name, and { expires: 7 } sets the cookie to expire in 7 days

    //             setAuth(true);

    //             toast.success('Login Sucessfully !', {
    //                 position: toast.POSITION.TOP_RIGHT
    //             });
    //         } else {
    //             setAuth(false)
    //             toast.error(parseRes)
    //         }
            
    //     } catch (error) {
    //         console.error(error.message)
    //     }
    // }

    return (
        <>
            <Container maxWidth="sm" style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
                <form style={{ width: '100%', marginBottom: '20px' }} onSubmit={onSubmitForm}>
                    <Typography variant="h4" gutterBottom style={{ marginBottom: '20px' }}>
                        Login
                    </Typography>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                type="email"
                                name="email"
                                label="Email"
                                variant="outlined"
                                value={email}
                                onChange={e => onChange(e)}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                fullWidth
                                type="password"
                                name="password"
                                label="Password"
                                variant="outlined"
                                value={password}
                                onChange={e => onChange(e)}
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                        size="large"
                        style={{ marginTop: '16px' }}
                    >
                        Submit
                    </Button>
                    
                </form>
                <Link to="/register">Register</Link>
            </Container>
        </>
        )
}

export default Login;
