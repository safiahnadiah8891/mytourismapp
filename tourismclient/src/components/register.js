import React, {useState} from 'react';
import { Typography, TextField, Button, Container, Grid } from '@mui/material';
import Cookies from 'js-cookie';
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';

function Register({setAuth}) {

    const [inputs,setInputs] = useState({
        email: "",
        password: "",
        name: ""
    })

    const {email, password, name} = inputs;

    const onChange = e => {
        setInputs({ ...inputs, [e.target.name]: e.target.value});
    }

    const onSubmitForm = async(e) => {
        e.preventDefault()

        try {

            const body = { email, password, name }
            const response = await fetch("http://localhost:5000/auth/register", {
                method: "POST",
                headers: { "Content-Type": "application/json"},
                body: JSON.stringify(body)
            });

            const parseRes = await response.json()

            if (parseRes.jwtToken) {
                const token = parseRes.jwtToken; // Replace with your actual token
                Cookies.set('token', token, { expires: 7 }); // 'token' is the cookie name, and { expires: 7 } sets the cookie to expire in 7 days

                setAuth(true);
                toast.success('Login Sucessfully !', {
                    position: toast.POSITION.TOP_RIGHT
                });
            } else {
                setAuth(false)
                toast.error(parseRes)
            }

           
        } catch (error) {
            console.error(error.message)
        }
    }

    return (
        <Container maxWidth="sm" style={{ display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', height: '100vh' }}>
            <form style={{ width: '100%', marginBottom: '20px' }} onSubmit={onSubmitForm}>
                <Typography variant="h4" gutterBottom style={{ marginBottom: '20px' }}>
                    Register
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            type="email"
                            name="email"
                            label="Email"
                            variant="outlined"
                            value={email}
                            onChange={e => onChange(e)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            type="password"
                            name="password"
                            label="Password"
                            variant="outlined"
                            value={password}
                            onChange={e => onChange(e)}
                        />
                    </Grid>
                    <Grid item xs={12}>
                        <TextField
                            fullWidth
                            type="text"
                            name="name"
                            label="Name"
                            variant="outlined"
                            value={name}
                            onChange={e => onChange(e)}
                        />
                    </Grid>
                </Grid>
                <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    fullWidth
                    size="large"
                    style={{ marginTop: '16px' }}
                >
                    Register
                </Button>
            </form>
            <Link to="/login">Login</Link>
        </Container>
    );
}

export default Register;
