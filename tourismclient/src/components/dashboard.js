import React, { useState, useEffect } from 'react';
import Cookies from 'js-cookie';
import { toast } from 'react-toastify';
import axios from 'axios'; // Import Axios

function Dashboard({ setAuth }) {

    const [name, setName] =  useState("")

    async function getName() {
        try {
            const response = await axios.get("http://localhost:5000/dashboard/", {
                headers: {
                    token: Cookies.get('token'),
                }
            });

            const parseRes = response.data; // Axios automatically parses JSON

            setName(parseRes.user_name);

        } catch (error) {
            console.error(error.message);
        }
    }

    // async function getName() {
    //     try {
    //         const response = await fetch("http://localhost:5000/dashboard/", {
    //             method:"GET",
    //             headers: {
    //                 token: Cookies.get('token'),
    //             }
    //         });

    //         const parseRes = await response.json()

    //         setName(parseRes.user_name);
            
    //     } catch (error) {
    //         console.error(error.message);
    //     }
    // }

    const logout = (e) => {
        e.preventDefault();
        Cookies.remove('token');
        setAuth(false);
        toast.success("Logged Out Successfully !");
    }

    useEffect(() => {
        getName();
    }, [])

    return (
        <>
            <h1>Dashboard {name}</h1>
            <button onClick={e => logout(e)}>Logout</button>
        </>
    )
}

export default Dashboard;